var sqlite3 = require('sqlite3').verbose()

const SRCDB = 'db.sqlite'

const db = new sqlite3.Database(SRCDB, (err) => {
  if (err) {
    console.error(err.message)
    throw err
  } else {
    console.log('Connected to database.')
    db.run(`CREATE TABLE user (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name text,
            email text UNIQUE,
            password text,
            CONSTRAINT email_unique UNIQUE (email)
            )`,
    (err) => {
      if (err) {
        console.log('Table already created. Skipping...')
      }
    })
  }
})

module.exports = db
